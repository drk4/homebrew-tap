# Installation #

-   `brew tap drk4/homebrew-tap https://bitbucket.org/drk4/homebrew-tap/`
-   `brew install screen_ruler`
-   `brew install tic_tac_toe`


# To get the new sha256 hash

- `brew install wget`
- `wget (url)`
- `shasum -a 256 (file)`
